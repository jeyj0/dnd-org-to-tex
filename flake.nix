{
  description = "A command-line tool to help with orgmode-based dnd notes.";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:

    flake-utils.lib.eachDefaultSystem
      (system:
        let pkgs = nixpkgs.legacyPackages.${system}; in
        rec {
          defaultPackage = packages.dnd-org;

          packages = {
            ghc = pkgs.haskellPackages.ghcWithPackages (hpkgs: with hpkgs; [
              split
              extra
              generic-lens
              directory
              deepseq

              parsec

              # org-mode
              containers
              filepath
              hashable
              parser-combinators
              base
              megaparsec
              text

              # tests
              hspec
              QuickCheck
              either
            ]);

            dnd-org = pkgs.stdenv.mkDerivation {
              name = "dnd-org";
              src = self;
              checkPhase = "make spec";
              buildPhase = "make build";
              installPhase = ''
                mkdir -p $out/bin
                cp out/main $out/bin/dnd-org
              '';

              buildInputs = with pkgs; [
                packages.ghc
                gnumake
              ];
            };
          };

          devShell = pkgs.mkShell {
            buildInputs = with pkgs; [
              packages.ghc
              haskellPackages.haskell-language-server
            ];
          };
        }
      );
}
