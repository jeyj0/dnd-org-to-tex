#!/usr/bin/env bash
set -e

dir="$1"

if [ $# -eq 0 ]; then
    echo "Usage: ./test_with_real_files.sh <dir>";
    exit 1;
fi

make build-fast

echo ""

find $dir -name "*.org" | while read file; do
    if ! cat "$file" | ./out/main >/dev/null; then
        echo "The above ERROR occurred in $file"
	echo ""
    fi
done
