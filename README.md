# dnd-org-to-tex

A binary that takes an orgmode file on stdin and converts it into a pdflatex-compatible file.

## Usage

1. build the project with the below instructions
2. install https://github.com/rpgtex/DND-5e-LaTeX-Template/
3. pipe an orgmode file into the project binary and save that as a `.tex` file
4. use `pdflatex` to compile the `.tex` file into a `.pdf` file

## Build Requirements

- nix (https://nixos.org/download.html)

### Optional

**lorri** (https://github.com/target/lorri) + **direnv** (https://direnv.net/)

If using lorri and direnv, just make sure that the current environment has been loaded in the shell. You can then only use the part of the `nix-shell` commands between `""` below, instead of the full commands.

## Build instructions

Run `nix-shell --pure --run "make"` instead. The resulting binary is in `./out/main`.

Alternatively, run `nix-build default.nix`, which will put the resulting binary in `./result/bin/dnd-org-to-tex`.

## Testing with `test_with_real_files.sh`

Run `nix-shell --pure --run "./test_with_real_files.sh $DIRNAME"`, where `$DIRNAME` is a directory containing orgmode files (searches recursively).
