module Util where

infixl 8 |>
(|>) :: a -> (a -> b) -> b
a |> f = f a
{-# INLINE (|>) #-}
