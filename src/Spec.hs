module Spec where

import           Test.Hspec

import           Data.Either.Combinators (fromRight')
import           Data.List

import           Main (convert)
import qualified OrgSpec

main :: IO ()
main = hspec $ do
  describe "Org" OrgSpec.spec
  describe "Main" spec

spec :: Spec
spec = do
  describe "convert" $ do
    it "weird characters" $ do
      -- when
      let latex = convert "‘experts’"
      -- then
      latex `shouldBe` Right "‘experts’"

    describe "extracts title" $ do
      it "UPPERCASE" $ do
        -- when
        let latex = convert "#+TITLE: My awesome title"

        -- then
        latex `shouldBe` Right "\\chapter{My awesome title}"

      it "lowercase" $ do
        -- when
        let latex = convert "#+title: Another awesome title"

        -- then
        latex `shouldBe` Right "\\chapter{Another awesome title}"

      it "miXEd caSE" $ do
        -- when
        let latex = convert "#+tItLe: Best one"

        -- then
        latex `shouldBe` Right "\\chapter{Best one}"

    it "handles bold text" $ do
      -- when
      let latex = convert "This has *bold text*."

      -- then
      latex `shouldBe` Right "This has \\textbf{bold text}."

    it "handles italic text" $ do
      -- when
      let latex = convert "This has /italic text/."

      -- then
      latex `shouldBe` Right "This has \\textit{italic text}."

    it "handles monospace text" $ do
      -- when
      let latex = convert "This has ~monospace text~."

      -- then
      latex `shouldBe` Right "This has \\texttt{monospace text}."

    describe "handles links" $ do
      it "with destination and text" $ do
        -- when
        let latex = convert "This has a [[destination][complex link]]."

        -- then
        latex `shouldBe` Right "This has a \\emph{\\textcolor{rulered}{complex link\\ExtLink}}."

      it "without separate destination" $ do
        -- when
        let latex = convert "This has a [[simple link]]."

        -- then
        latex `shouldBe` Right "This has a \\emph{\\textcolor{rulered}{simple link}}."

    describe "handles tables" $ do
      it "simple table" $ do
        -- when
        let latex = convert "| header 1 | header 2 |\n| cell 1 | cell 2 |"

        -- then
        latex `shouldBe` Right
          "\\begin{DndTable}[]{lX}\nheader 1 & header 2 \\\\\ncell 1 & cell 2 \\\\\n\\end{DndTable}"

      it "with empty column" $ do
        -- when
        let latex = convert "| header 1 |  | header 3 |"

        -- then
        latex `shouldBe` Right "\\begin{DndTable}[]{lXX}\nheader 1 &  & header 3 \\\\\n\\end{DndTable}"

      describe "automatically assigns a table formatting" $ do
        it "1 column" $ do
          -- when
          let latex = convert "| h1 |"
          -- then
          latex `shouldBe` Right
            "\\begin{DndTable}[]{X}\nh1 \\\\\n\\end{DndTable}"

        it "2 columns" $ do
          -- when
          let latex = convert "| h1 | h2 |"
          -- then
          latex `shouldBe` Right
            "\\begin{DndTable}[]{lX}\nh1 & h2 \\\\\n\\end{DndTable}"

        it "3 columns" $ do
          -- when
          let latex = convert "| h1 | h2 | h3 |"
          -- then
          latex `shouldBe` Right
            "\\begin{DndTable}[]{lXX}\nh1 & h2 & h3 \\\\\n\\end{DndTable}"

        it "4 columns" $ do
          -- when
          let latex = convert "| h1 | h2 | h3 | h4 |"
          -- then
          latex `shouldBe` Right
            "\\begin{DndTable}[]{lXXX}\nh1 & h2 & h3 & h4 \\\\\n\\end{DndTable}"

    it "handles headers" $ do
      -- when
      let latex = convert $
            "* First-level header\n" <>
            "** Second-level header\n" <>
            "** Second-level area :area:\n" <>
            "*** Third-level header\n" <>
            "*** Third-level area :subarea:\n" <>
            "**** Fourth-level header\n" <>
            "***** Fifth-level header"

      -- then
      latex `shouldBe` Right
           ( "\\section{First-level header}\n"
          <> "\\subsection{Second-level header}\n\n"
          <> "\\DndArea{Second-level area}\n"
          <> "\\newcounter{orgleveli}\\setcounter{orgleveli}{0}\\subsubsection{Third-level header}\n\n"
          <> "\\setcounter{orgleveli}{0}\\DndSubArea{Third-level area}\n"
          <> "\\vspace{1em}\\refstepcounter{orgleveli}\\noindent\\textbf{\\theorgleveli\\hspace{0.5em}Fourth-level header}\\break\n"
          <> "\\newcounter{orglevelii}[orgleveli]\\vspace{1em}\\refstepcounter{orglevelii}\\noindent\\textbf{\\theorgleveli.\\theorglevelii\\hspace{0.5em}Fifth-level header}\\break"
           )

    it "handles complex header structure" $ do
      -- when
      let latex = convert $ unlines
            [ "* 1"
            , "** 1.1"
            , "** 1.2"
            , "* 2"
            , "** 2.1"
            , "*** 2.1.1"
            , "content for 2.1.1"
            , "*** 2.1.2"
            , "** 2.2"
            , "*** 2.2.1"
            , "**** 2.2.1.1"
            , "**** 2.2.1.2"
            , "*** 2.2.2"
            , "**** 2.2.2.1"
            , "***** 2.2.2.1.1"
            , "some content"
            , "***** 2.2.2.1.2"
            , "**** 2.2.2.2"
            , "***** 2.2.2.2.1"
            , "*** 2.2.3"
            , "**** 2.2.3.1"
            , "***** 2.2.3.1.1"
            ]
      -- then
      latex `shouldBe` Right
         ( "\\section{1}\n"
        <> "\\subsection{1.1}\n\n"
        <> "\\subsection{1.2}\n\n"
        <> "\\section{2}\n"
        <> "\\subsection{2.1}\n"
        <> "\\newcounter{orgleveli}\\setcounter{orgleveli}{0}\\subsubsection{2.1.1}\n"
        <> "content for 2.1.1\n\n"
        <> "\\setcounter{orgleveli}{0}\\subsubsection{2.1.2}\n\n"
        <> "\\subsection{2.2}\n"
        <> "\\setcounter{orgleveli}{0}\\subsubsection{2.2.1}\n"
        <> "\\vspace{1em}\\refstepcounter{orgleveli}\\noindent\\textbf{\\theorgleveli\\hspace{0.5em}2.2.1.1}\\break\n\n"
        <> "\\vspace{1em}\\refstepcounter{orgleveli}\\noindent\\textbf{\\theorgleveli\\hspace{0.5em}2.2.1.2}\\break\n\n"
        <> "\\setcounter{orgleveli}{0}\\subsubsection{2.2.2}\n"
        <> "\\vspace{1em}\\refstepcounter{orgleveli}\\noindent\\textbf{\\theorgleveli\\hspace{0.5em}2.2.2.1}\\break\n"
        <> "\\newcounter{orglevelii}[orgleveli]"
        <> "\\vspace{1em}\\refstepcounter{orglevelii}\\noindent\\textbf{\\theorgleveli.\\theorglevelii\\hspace{0.5em}2.2.2.1.1}\\break\n"
        <> "some content\n\n"
        <> "\\vspace{1em}\\refstepcounter{orglevelii}\\noindent\\textbf{\\theorgleveli.\\theorglevelii\\hspace{0.5em}2.2.2.1.2}\\break\n\n"
        <> "\\vspace{1em}\\refstepcounter{orgleveli}\\noindent\\textbf{\\theorgleveli\\hspace{0.5em}2.2.2.2}\\break\n"
        <> "\\vspace{1em}\\refstepcounter{orglevelii}\\noindent\\textbf{\\theorgleveli.\\theorglevelii\\hspace{0.5em}2.2.2.2.1}\\break\n\n"
        <> "\\setcounter{orgleveli}{0}\\subsubsection{2.2.3}\n"
        <> "\\vspace{1em}\\refstepcounter{orgleveli}\\noindent\\textbf{\\theorgleveli\\hspace{0.5em}2.2.3.1}\\break\n"
        <> "\\vspace{1em}\\refstepcounter{orglevelii}\\noindent\\textbf{\\theorgleveli.\\theorglevelii\\hspace{0.5em}2.2.3.1.1}\\break"
         )

    describe "handles lists" $ do
      it "basic" $ do
        -- when
        let latex = convert "- item 1\n- item 2"

        -- then
        latex `shouldBe` Right "\\begin{itemize}\n\\item{item 1}\n\\item{item 2}\n\\end{itemize}"

      it "nested" $ do
        -- when
        let latex = convert "- item 1\n  - subitem 1\n  - subitem 2\n- item 2"

        -- then
        latex `shouldBe` Right ("\\begin{itemize}\n" <>
          "\\item{item 1}\n" <>
            "\\begin{itemize}\n" <>
              "\\item{subitem 1}\n" <>
              "\\item{subitem 2}\n" <>
            "\\end{itemize}\n" <>
            "\\item{item 2}\n" <>
          "\\end{itemize}")

      it "definition" $ do
        -- when
        let latex = convert "- Name :: definition"

        -- then
        latex `shouldBe` Right "\\paragraph{Name} definition"

      it "definition with sublist" $ do
        -- when
        let latex = convert "- Name :: definition\n  - subList item"

        -- then
        latex `shouldBe` Right "\\paragraph{Name} definition\n\\begin{itemize}\n\\item{subList item}\n\\end{itemize}"

    it "handles a simple document" $ do
      -- when
      let latex = convert $ unlines
            [ "#+TITLE: My awesome NPC"
            , ""
            , "- Gender :: Male"
            , "- Race :: Elf"
            , "- Location :: Somewhere in the world"
            , ""
            , "* Some important headline"
            , "With text inside"
            , ""
            , "** And a second-level headline"
            ]

      -- then
      latex `shouldBe` Right (intercalate "\n"
        [ "\\chapter{My awesome NPC}"
        , "\\paragraph{Gender} Male"
        , ""
        , "\\paragraph{Race} Elf"
        , ""
        , "\\paragraph{Location} Somewhere in the world"
        , ""
        , "\\section{Some important headline}"
        , "With text inside"
        , ""
        , "\\subsection{And a second-level headline}"
        ])

    it "handles a document with a properties drawer" $ do
      -- when
      let latex = convert $ unlines
            [ ":PROPERTIES:"
            , ":ID:     some-uuid"
            , ":END:"
            , "#+TITLE: a title"
            , "some content"
            ]
      -- then
      latex `shouldBe` Right (intercalate "\n"
        [ "\\chapter{a title}"
        , "some content"
        ])

    it "renders attached image" $ do
      -- when
      let latex = convert $ unlines
            [ ":PROPERTIES:"
            , ":ID:       dab4e1ae-5e82-47da-9a3e-9b77aa9ff85d"
            , ":END:"
            , "#+TITLE: Shadowtrick (Shortsword)"
            , "#+filetags: item attunement martial weapon"
            , ""
            , "#+ATTR_ORG: :width 200"
            , "[[attachment:shadowtrick.jpg]]"
            ]
      -- then
      latex `shouldBe` Right (intercalate "\n"
        [ "\\chapter{Shadowtrick (Shortsword)}"
        , ""
        , "\\begin{wrapfigure}{r}{.5\\textwidth}" -- {\\columnwidth}"
        , "\\includegraphics[width=.5\\textwidth]{~/org/attachments/da/b4e1ae-5e82-47da-9a3e-9b77aa9ff85d/shadowtrick.jpg}"
        , "\\end{wrapfigure}"
        ])
