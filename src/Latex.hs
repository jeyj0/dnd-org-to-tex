{-# LANGUAGE LambdaCase #-}
module Latex where

import qualified Data.List.NonEmpty as NE
import           Prelude hiding (unlines, putStrLn, replicate)
import           Data.Map.Strict hiding (map, mapMaybe, foldr, foldl, filter)
import           Data.Maybe
import           Data.List
import           Data.Char

import           Util
import           Org

convertOrgASTToText :: OrgFile -> String
convertOrgASTToText orgFile = asText (convertToLatexAST orgFile)

preText :: String
preText = intercalate "\n"
  -- [ "\\documentclass[bg=none,10pt,twocolumn,a4paper,openany,nomultitoc,nodeprecatedcode]{book}"
  -- [ "\\documentclass[bg=none,10pt,a4paper,openany,nomultitoc,nodeprecatedcode]{book}"
  [ "\\documentclass[bg=none,10pt,openany,nomultitoc,nodeprecatedcode]{book}"
  , "\\pagenumbering{gobble}"
  , ""
  , "\\usepackage[layout=true]{dnd}"
  , "\\usepackage[paperheight=105mm,paperwidth=74mm]{geometry}"
  , ""
  , "\\fancyhf{}"
  , ""
  , "\\usepackage[english]{babel}"
  , "\\usepackage[utf8]{inputenc}"
  , "\\usepackage[singlelinecheck=false]{caption}"
  , "\\usepackage{lipsum}"
  , "\\usepackage{listings}"
  , "\\usepackage{shortvrb}"
  , "\\usepackage{stfloats}"
  , "\\usepackage{hyperref}"
  , "\\usepackage{multicol}"
  , "\\usepackage{graphicx}"
  , "\\usepackage{wrapfig}"
  , ""
  , "\\usepackage{amsmath}"
  , "\\usepackage{tikz}"
  , "\\newcommand{\\ExtLink}{"
  , "    \\includegraphics[height=5pt]{ext_link.png}"
  , "}"
  , ""
  , "\\titleformat{\\chapter}"
  , "    {\\DndFontChapter} % format"
  , "    {}"
  , "    {0pt}"
  , "    {\\DndContour} % before-code"
  , "\\begin{document}"
  ]

postText :: String
postText = "\\end{document}"

-- LATEX

---- TYPES

data LatexDoc = LatexDoc
  { level :: Int
  , isArea :: Bool
  , title :: Maybe String
  , blocks :: [LatexBlock]
  , latexDocs :: [LatexDoc]
  , nodeid :: Maybe String
  }

data LatexBlock
  = SimpleText String
  | LatexTable Int [LatexRow]
  | LatexList LatexList

type LatexRow = [String]

data LatexList
  = BasicList [(String, Maybe LatexList)]
  | ParagraphList [(String, String, Maybe LatexList)]

---- AST CONVERSION

convertToLatexAST :: OrgFile -> LatexDoc
convertToLatexAST orgFile = LatexDoc
  { title = title'
  , level = 0
  , isArea = False
  , blocks = blocks'
  , latexDocs = sections'
  , nodeid = nodeid
  }
  where
    meta = mapKeys (map toLower) $ orgMeta orgFile
    doc = orgDoc orgFile

    title' = meta !? "title"
    nodeid = idFromProperties $ orgProperties orgFile

    blocks' = map (fromOrgBlock nodeid) $ docBlocks doc
    sections' = map (fromOrgSection nodeid 1) $ docSections doc

idFromProperties :: Maybe PropertyDrawer -> Maybe String
idFromProperties Nothing = Nothing
idFromProperties (Just drawer) =
  let
    idProps = drawer |> filter
      (\p -> case p of
               DrawerProperty "ID" _ -> True
               _ -> False)
  in case idProps of
    [] -> Nothing
    [DrawerProperty "ID" nodeid] -> Just nodeid
    _ -> error "This should be unreachable"
  
fromOrgSection :: Maybe String -> Int -> Section -> LatexDoc
fromOrgSection id level section = LatexDoc
  { level = level
  , isArea = "area" `elem` tags || "subarea" `elem` tags
  , title = Just $ fromTextElements id heading
  , blocks = map (fromOrgBlock id) $ docBlocks doc
  , latexDocs = map (fromOrgSection id $ level + 1) $ docSections doc
  , nodeid = undefined
  }
  where
    heading = sectionHeading section
    tags = sectionTags section
    doc = sectionDoc section

fromOrgBlock :: Maybe String -> ([AffiliatedKeyword], Block) -> LatexBlock
fromOrgBlock id (keywords, Paragraph textElements) = SimpleText $ fromTextElements id textElements
fromOrgBlock id (keywords, Table rows') =
  LatexTable columnCount $ map fromOrgRow rows
  where
    rows = NE.toList rows'

    fromOrgRow :: Row -> LatexRow
    fromOrgRow Break = undefined
    fromOrgRow (Row cols) = map fromOrgCol $ NE.toList cols

    fromOrgCol :: Column -> String
    fromOrgCol Empty = ""
    fromOrgCol (Column elems) = fromTextElements id elems

    columnCount :: Int
    columnCount = maximum $ map rowLength rows

    rowLength :: Row -> Int
    rowLength Break = 1
    rowLength (Row cols) = length cols
fromOrgBlock id (keywords, List items) =
  LatexList $ latexList False items
  where
    hasNonDescription = any $ \case
      DescriptionItem {} -> False
      _ -> True

    asBasicItem :: Item -> (String, Maybe LatexList)
    asBasicItem (DescriptionItem name description mSubListItems) =
      ("\\textbf{" <> fromTextElements id name <> ".} " <> fromTextElements id description, Nothing)
    asBasicItem (Item content mSubListItems) =
      (fromTextElements id content, latexList True <$> mSubListItems)

    asDescriptionItem :: Item -> (String, String, Maybe LatexList)
    asDescriptionItem (DescriptionItem name description mSubListItems) =
      (fromTextElements id name, fromTextElements id description, latexList True <$> mSubListItems)
    asDescriptionItem _ = undefined

    latexList :: Bool -> ListItems -> LatexList
    latexList isSubList (ListItems items) =
      if isSubList || hasNonDescription (NE.toList items)
        then BasicList $ map asBasicItem $ NE.toList items
        else ParagraphList $ map asDescriptionItem $ NE.toList items
fromOrgBlock id (_, DynamicBlock {}) = undefined
fromOrgBlock id (_, BlockElement _) = undefined

fromTextElements' :: Maybe String -> [TextElement] -> String
fromTextElements' id = foldr ((<>) . fromTextElement id) ""

fromTextElements :: Maybe String -> NE.NonEmpty TextElement -> String
fromTextElements id = fromTextElements' id . NE.toList

fromTextElement :: Maybe String -> TextElement -> String
fromTextElement (Just id) (Link (URL ('a':'t':'t':'a':'c':'h':'m':'e':'n':'t':':':filename)) Nothing) =
  let
    attachmentDirPath = "~/org/attachments/" <> [head id] <> [head (tail id)] <> "/" <> tail (tail id) <> "/"
    attachmentPath = attachmentDirPath <> filename
    isImage filename = case reverse filename of
      'g':'n':'p':'.':_ -> True
      'g':'p':'j':'.':_ -> True
      'g':'e':'p':'j':'.':_ -> True
      _ -> False
  in
  if isImage filename
  then intercalate "\n"
    [ ""
    , "\\begin{wrapfigure}{r}{.5\\textwidth}"
    , "\\includegraphics[width=.5\\textwidth]{" <> attachmentPath <> "}"
    , "\\end{wrapfigure}"
    ]
  else basicLink (Just id) ("attachment:" <> filename) Nothing
fromTextElement id (Link (URL destination) description) = basicLink id destination description
fromTextElement id (Bold elems) = "\\textbf{" <> fromTextElements id elems <> "}"
fromTextElement id (Italic elems) = "\\textit{" <> fromTextElements id elems <> "}"
fromTextElement id (Monospace elems) = "\\texttt{" <> fromTextElements id elems <> "}"
fromTextElement id (Verbatim elems) = "\\texttt{" <> fromTextElements id elems <> "}"
fromTextElement id (Strikethrough elems) = undefined
fromTextElement id (Plain s) = s
fromTextElement id (Punct s) = s
fromTextElement id (Whitespace _) = " "

basicLink :: Maybe String -> String -> Maybe (NE.NonEmpty TextElement) -> String
basicLink id destination description =
  "\\emph{\\textcolor{rulered}{" <>
  (case description of
    Nothing -> destination
    Just description -> fromTextElements id description <> "\\ExtLink") <>
  "}}"
  
---- AST TO TEXT

asText :: LatexDoc -> String
asText =
  snd . asText' 0
  where
    asText' :: Int -> LatexDoc -> (Int, String)
    asText' createdCounters doc =
      (fst subDocs, text)
      where
        (titleCounter, mtitle) = case titleLevelFunction <$> title doc of
          Nothing -> (0, Nothing)
          Just (c, s) -> (c, Just s)
        mtextsFromBlocks = map (Just . latexBlockToText) (blocks doc)

        subDocs :: (Int, [Maybe String])
        subDocs = foldl (\(createdCounters, mstrings) doc ->
                           let
                             (counter, string) = asText' createdCounters doc
                           in
                           (max counter createdCounters, mstrings ++ [Just string])) (titleCounter, []) (latexDocs doc)

        text = case mtitle of
          Nothing -> content
          Just title -> intercalate "\n" $ catMaybes
            [ Just title
            , case content of
                [] -> Nothing
                c -> Just c
            ]
          where
            content = intercalate "\n\n" $ catMaybes $ mtextsFromBlocks ++ snd subDocs

        titleLevelFunction :: String -> (Int, String)
        titleLevelFunction t = case level doc of
          0 -> (createdCounters, "\\chapter{" <> t <> "}")
          1 -> (createdCounters, "\\section{" <> t <> "}")
          2 -> (createdCounters, "\\" <> (if isArea doc then "DndArea" else "subsection") <> "{" <> t <> "}")
          3 ->
            let
              afterText = "\\setcounter{orgleveli}{0}\\" <> (if isArea doc then "DndSubArea" else "subsubsection") <> "{" <> t <> "}"
            in if createdCounters == 0
            then (5, "\\newcounter{orgleveli}" <> afterText)
            else (createdCounters, afterText)
          lvl ->
            let
              levelCounter lvl = "orglevel" <> ['i' | _ <- [0..lvl-4]]
              previousLevelCounters = [levelCounter l | l <- [4..lvl]]
              currentLevelCounter = last previousLevelCounters

              afterText = 
                "\\vspace{1em}\\refstepcounter{" <> currentLevelCounter <>
                "}\\noindent\\textbf{" <> intercalate "." (map (\c -> "\\the" <> c) previousLevelCounters) <>
                "\\hspace{0.5em}" <> t <> "}\\break"
            in if lvl >= createdCounters
            then (createdCounters + 1, "\\newcounter{" <> levelCounter lvl <> "}[" <> levelCounter (lvl-1) <> "]" <> afterText)
            else (createdCounters, afterText)

latexBlockToText :: LatexBlock -> String
latexBlockToText (SimpleText text) = text
latexBlockToText (LatexTable columnCount rows) = renderTable columnCount rows
latexBlockToText (LatexList latexList) = renderList latexList

renderTable :: Int -> [LatexRow] -> String
renderTable columnCount rows =
  "\\begin{DndTable}[]{" <> tableFormatting columnCount <> "}\n" <>
  intercalate "\n" (map renderRow rows) <>
  "\n\\end{DndTable}"
  where
    tableFormatting 1 = "X"
    tableFormatting columnCount
      | columnCount < 1 = error "Cannot render a table with less than one column."
      | otherwise = "l" <> replicate (columnCount - 1) 'X'
    renderRow :: LatexRow -> String
    renderRow row = intercalate " & " row <> " \\\\"

renderList :: LatexList -> String
renderList (BasicList items) =
  "\\begin{itemize}\n" <>
  intercalate "\n" (map
    (\(content, mSubList) -> "\\item{" <> content <> "}" <> maybe ""
      (\subList -> "\n" <> renderSubList subList) mSubList)
    items) <>
  "\n\\end{itemize}"
renderList (ParagraphList items) =
  intercalate "\n\n" $ map
    (\(n, d, s) -> "\\paragraph{" <> n <> "} " <> d <> maybe ""
      (\subList -> "\n" <> renderSubList subList) s)
    items

renderSubList :: LatexList -> String
renderSubList (BasicList items) = renderList $ BasicList items
renderSubList _ = undefined
