{-# LANGUAGE FlexibleContexts #-}
module OrgSpec where

import           Test.Hspec
-- import           Test.QuickCheck

import           Data.Functor.Identity ( Identity(Identity), Identity )
import           Text.Parsec hiding (Empty)
import           Text.Parsec.Error (ParseError)
import           Data.Either.Combinators (fromRight')
import qualified Data.Map.Strict as M
import qualified Data.List.NonEmpty as NE

import           Org

parse'' :: Stream s Identity t =>
  u -> ParsecT s u Identity a -> s -> Either ParseError a
parse'' st p = runParser p st ""

parse' :: ParsecT String ParserState Identity a
  -> String -> Either ParseError a
parse' = parse'' defaultParserState

sectionNamed name = sectionNamedWithSections name []

sectionNamedWithContent :: String -> String -> Section
sectionNamedWithContent name content = sectionNamedWithSectionsAndContent name [] (Just content)

sectionNamedWithSections :: String -> [Section] -> Section
sectionNamedWithSections name sections = sectionNamedWithSectionsAndContent name sections Nothing
  
sectionNamedWithSectionsAndContent :: String -> [Section] -> Maybe String -> Section
sectionNamedWithSectionsAndContent name sections mcontent = Section
  { sectionHeading = NE.fromList [Plain name]
  , sectionPlanningInfo = emptyPlanningInfo
  , sectionProperties = Nothing
  , sectionTags = []
  , sectionTodoKeyword = Nothing
  , sectionPriority = Nothing
  , sectionDoc = OrgDoc
    { docBlocks = case mcontent of
        Just content ->
          [ ([], Paragraph $ NE.fromList [ Plain content ])
          ]
        Nothing -> []
    , docSections = sections
    }
  }

spec :: Spec
spec = do
  describe "full" $ do
    it "simple doc" $ do
      -- when
      let res = fromRight' $ parse' orgFileParser
            "#+TITLE: My simple document\n\
            \#+roam_tags:\n\
            \\n\
            \With one *paragraph* of one line."
      -- then
      res `shouldBe` OrgFile
        { orgMeta = M.fromList [("TITLE", "My simple document"), ("roam_tags", "")]
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks =
            [ ([], Paragraph $ NE.fromList
              [ Plain "With"
              , Whitespace " "
              , Plain "one"
              , Whitespace " "
              , Bold (NE.fromList [Plain "paragraph"])
              , Whitespace " "
              , Plain "of"
              , Whitespace " "
              , Plain "one"
              , Whitespace " "
              , Plain "line"
              , Punct "."
              ])
            ]
          , docSections = []
          }
        }

    it "handles root property drawer" $ do
      -- when
      let res = parse' orgFileParser $ unlines
            [ ":PROPERTIES:"
            , ":NORMAL: FIRST"
            , ":NORMAL+: second"
            , ":empty:"
            , ":empty+:"
            , ":END:"
            ]
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgDoc = emptyDoc
        , orgProperties = Just
          [ DrawerProperty "NORMAL" "FIRST"
          , DrawerPropertyPlus "NORMAL" "second"
          , EmptyDrawerProperty "empty"
          , EmptyDrawerPropertyPlus "empty"
          ]
        }

    it "handles root property drawer with content afterwards" $ do
      -- when
      let res = parse' orgFileParser $ unlines
            [ ":PROPERTIES:"
            , ":ID: some id"
            , ":END:"
            , "#+TITLE: a title"
            , "#+filetags: one two"
            , ""
            , "some content"
            , ""
            , "* section"
            ]
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.fromList [("TITLE", "a title"), ("filetags", "one two")]
        , orgProperties = Just
          [ DrawerProperty "ID" "some id"
          ]
        , orgDoc = OrgDoc
          { docBlocks = [([], Paragraph $ NE.fromList [ Plain "some", Whitespace " ", Plain "content" ])]
          , docSections =
            [ sectionNamed "section"
            ]
          }
        }

    it "handles file without meta" $ do
      -- when
      let res = parse' orgFileParser "Some text"
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks = [([], Paragraph $ NE.fromList [ Plain "Some", Whitespace " ", Plain "text" ] )]
          , docSections = []
          }
        }

    -- it "handles results" $ do
    --   -- when
    --   let res = parse' orgFileParser $ unlines
    --         [ "* Headline"
    --         , "#+BEGIN_SRC haskell"
    --         , ":{"
    --         , "data MyRecord = MyRecord"
    --         , ":}"
    --         , "#+END_SRC"
    --         , ""
    --         , "#+RESULTS:"
    --         ]
    --   -- then
    --   res `shouldBe` Right OrgFile
    --     { orgMeta = M.empty
    --     , orgProperties = Nothing
    --     , orgDoc = OrgDoc -- TODO
    --       { docBlocks = []
    --       , docSections = []
    --       }
    --     }

    it "handles weird characters" $ do
      -- when
      let res = parse' orgFileParser "‘experts’"
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks = [([], Paragraph $ NE.fromList [Punct "‘", Plain "experts", Punct "’"])]
          , docSections = []
          }
        }

    it "handles file without meta but formatting" $ do
      -- when
      let res = parse' orgFileParser "Some *bold* text"
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks =
            [ ([], Paragraph $ NE.fromList
              [ Plain "Some"
              , Whitespace " "
              , Bold $ NE.fromList
                [ Plain "bold"
                ]
              , Whitespace " "
              , Plain "text"
              ])
            ]
          , docSections = []
          }
        }

    it "handles empty file" $ do
      -- when
      let res = parse' orgFileParser ""
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = emptyDoc
        }

    it "handles non-meta file with headlines" $ do
      -- when
      let res = parse' orgFileParser
            "* H1"
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks = []
          , docSections =
            [ Section
              { sectionHeading = NE.fromList [Plain "H1"]
              , sectionPlanningInfo = emptyPlanningInfo
              , sectionDoc = emptyDoc
              , sectionTags = []
              , sectionTodoKeyword = Nothing
              , sectionPriority = Nothing
              , sectionProperties = Nothing
              }
            ]
          }
        }

    it "handles multiple newlines between meta and doc" $ do
      -- when
      let res = fromRight' $ parse' orgFileParser
            "#+k: v\n\
            \\n\
            \\n\
            \p"
      -- then
      res `shouldBe` OrgFile
        { orgMeta = M.fromList [("k", "v")]
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks = [([], Paragraph $ NE.fromList [ Plain "p" ] )]
          , docSections = []
          }
        }

    it "handles only table with no newline at end of file" $ do
      -- when
      let res = parse' orgFileParser
            "| H1 | H2 |\n\
            \| C1 | C2 |"
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks =
            [ ([], Table $ NE.fromList
              [ Row $ NE.fromList [Column $ NE.fromList [Plain "H1"], Column $ NE.fromList [Plain "H2"]]
              , Row $ NE.fromList [Column $ NE.fromList [Plain "C1"], Column $ NE.fromList [Plain "C2"]]
              ])
            ]
          , docSections = []
          }
        }

    it "handles only list with no newline at end of file" $ do
      -- when
      let res = parse' orgFileParser
            "- item1\n\
            \- item2"
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks =
            [ ([], List $ ListItems $ NE.fromList
              [ Item (NE.fromList [Plain "item1"]) Nothing
              , Item (NE.fromList [Plain "item2"]) Nothing
              ])
            ]
          , docSections = []
          }
        }

    it "simple doc with two paragraphs" $ do
      -- when
      let res = fromRight' $ parse' orgFileParser
            "#+TITLE: Two paragraphs\n\
            \\n\
            \This is the first paragraph\n\
            \\n\
            \This is the second paragraph."
      -- then
      res `shouldBe` OrgFile
        { orgMeta = M.fromList [("TITLE", "Two paragraphs")]
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks =
            [ ([], Paragraph $ NE.fromList
                [ Plain "This"
                , Whitespace " "
                , Plain "is"
                , Whitespace " "
                , Plain "the"
                , Whitespace " "
                , Plain "first"
                , Whitespace " "
                , Plain "paragraph"
                ])
            , ([], Paragraph $ NE.fromList
                [ Plain "This"
                , Whitespace " "
                , Plain "is"
                , Whitespace " "
                , Plain "the"
                , Whitespace " "
                , Plain "second"
                , Whitespace " "
                , Plain "paragraph"
                , Punct "."
                ])
            ]
          , docSections = []
          }
        }

    it "parses list immediately after paragraph" $ do
      -- when
      let res = parse' orgFileParser $ unlines
            [ "A paragraph"
            , "- list"
            ]
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docBlocks =
            [ ([], Paragraph $ NE.fromList
              [ Plain "A"
              , Whitespace " "
              , Plain "paragraph"
              ])
            , ([], List $ ListItems $ NE.fromList
              [ Item (NE.fromList [Plain "list"]) Nothing
              ])
            ]
          , docSections = []
          }
        }

    it "parses paragraph immediately after list" $ do
      -- when
      let res = parse' orgFileParser $ unlines
            [ "- list"
            , "Paragraph"
            ]
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docSections = []
          , docBlocks =
            [ ([], List $ ListItems $ NE.fromList
              [ Item (NE.fromList [Plain "list"]) Nothing
              ]
              )
            , ([], Paragraph $ NE.fromList
              [ Plain "Paragraph"
              ])
            ]
          }
        }

    it "parses table immediately after list" $ do
      -- when
      let res = parse' orgFileParser $ unlines
            [ "- list"
            , "| Table |"
            ]
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docSections = []
          , docBlocks =
            [ ([], List $ ListItems $ NE.fromList
              [ Item (NE.fromList [Plain "list"]) Nothing
              ])
            , ([], Table $ NE.fromList
              [ Row $ NE.fromList [Column $ NE.fromList [Plain "Table"]]
              ])
            ]
          }
        }

    it "parses paragraph immediately after table" $ do
      -- when
      let res = parse' orgFileParser $ unlines
            [ "| Table |"
            , "Paragraph"
            ]
      -- then
      res `shouldBe` Right OrgFile
        { orgMeta = M.empty
        , orgProperties = Nothing
        , orgDoc = OrgDoc
          { docSections = []
          , docBlocks =
            [ ([], Table $ NE.fromList
              [ Row $ NE.fromList [Column $ NE.fromList [Plain "Table"]]
              ])
            , ([], Paragraph $ NE.fromList [Plain "Paragraph"])
            ]
          }
        }

  it "parses top-level sections" $ do
    -- when
    let res = fromRight' $ parse' orgFileParser $ unlines
          [ "#+TITLE: Top-Level sections"
          , ""
          , "This is a paragraph at the very top."
          , ""
          , "* Top-Level heading"
          , "With a paragraph."
          ]
    -- then
    res `shouldBe` OrgFile
      { orgMeta = M.fromList [("TITLE", "Top-Level sections")]
      , orgProperties = Nothing
      , orgDoc = OrgDoc
        { docBlocks =
          [ ([], Paragraph $ NE.fromList
            [ Plain "This"
            , Whitespace " "
            , Plain "is"
            , Whitespace " "
            , Plain "a"
            , Whitespace " "
            , Plain "paragraph"
            , Whitespace " "
            , Plain "at"
            , Whitespace " "
            , Plain "the"
            , Whitespace " "
            , Plain "very"
            , Whitespace " "
            , Plain "top"
            , Punct "."
            ])
          ]
        , docSections =
          [ Section
            { sectionHeading = NE.fromList
              [ Plain "Top"
              , Punct "-"
              , Plain "Level"
              , Whitespace " "
              , Plain "heading"
              ]
            , sectionPlanningInfo = emptyPlanningInfo
            , sectionProperties = Nothing
            , sectionTags = []
            , sectionTodoKeyword = Nothing
            , sectionPriority = Nothing
            , sectionDoc = OrgDoc
              { docBlocks =
                [ ([], Paragraph $ NE.fromList
                  [ Plain "With"
                  , Whitespace " "
                  , Plain "a"
                  , Whitespace " "
                  , Plain "paragraph"
                  , Punct "."
                  ])
                ]
              , docSections = []
              }
            }
          ]
        }
      }

  describe "section" $ do
    it "handles a basic section" $ do
      -- when
      let res = fromRight' $ parse' section $ unlines
            [ "* Simple section"
            , "With content"
            ]
      -- then
      res `shouldBe` Section
        { sectionHeading = NE.fromList [Plain "Simple", Whitespace " ", Plain "section"]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionProperties = Nothing
        , sectionTags = []
        , sectionTodoKeyword = Nothing
        , sectionPriority = Nothing
        , sectionDoc = OrgDoc
          { docBlocks =
            [ ([], Paragraph $ NE.fromList
              [ Plain "With"
              , Whitespace " "
              , Plain "content"
              ])
            ]
          , docSections = []
          }
        }

    describe "with planning information" $ do
      it "scheduled; active timestamp" $ do
        -- when
        let res = parse' section $ unlines
              [ "* Headline"
              , "SCHEDULED: <1999-04-12 Wed>"
              ]
        -- then
        res `shouldBe` Right Section
          { sectionHeading = NE.fromList [Plain "Headline"]
          , sectionPlanningInfo = PlanningInfo
            { scheduled = Just ActiveTimestamp
              { year = 1999
              , month = April
              , day = Day12
              , dayOfWeek = "Wed"
              }
            }
          , sectionProperties = Nothing
          , sectionTags = []
          , sectionTodoKeyword = Nothing
          , sectionPriority = Nothing
          , sectionDoc = emptyDoc
          }

    it "handles section with newlines after headline" $ do
      -- when
      let res = fromRight' $ parse' section $ unlines
            [ "* Section"
            , ""
            , "Content"
            ]
      -- then
      res `shouldBe` Section
        { sectionHeading = NE.fromList [Plain "Section"]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionProperties = Nothing
        , sectionTags = []
        , sectionTodoKeyword = Nothing
        , sectionPriority = Nothing
        , sectionDoc = OrgDoc
          { docBlocks = [([], Paragraph $ NE.fromList [Plain "Content"])]
          , docSections = []
          }
        }

    it "handles back-to-back sections" $ do
      -- when
      let res = fromRight' $ parse' section $ unlines
            [ "* 1"
            , "** 11"
            , "** 12"
            , "*** 121"
            , "*** 122"
            , "**** 1221"
            , "**** 1222"
            , "***** 12221"
            , "***** 12222"
            , "*** 123"
            ]
      -- then
      res `shouldBe` sectionNamedWithSections "1"
        [ sectionNamed "11"
        , sectionNamedWithSections "12"
          [ sectionNamed "121"
          , sectionNamedWithSections "122"
            [ sectionNamed "1221"
            , sectionNamedWithSections "1222"
              [ sectionNamed "12221"
              , sectionNamed "12222"
              ]
            ]
          , sectionNamed "123"
          ]
        ]

    it "handles going back up a section after content when headlines could make bold text" $ do
      -- when
      let res = parse' section $ unlines
            [ "* 1"
            , "** 11"
            , "*** 111"
            , "**** 1111"
            , "content"
            , "*** 112"
            , "**** 1121"
            , "***** 11211"
            , "****** 112111"
            , "**** 1122"
            ]
      -- then
      res `shouldBe` Right (sectionNamedWithSections "1"
        [ sectionNamedWithSections "11"
          [ sectionNamedWithSections "111"
            [ sectionNamedWithContent "1111" "content"
            ]
          , sectionNamedWithSections "112"
            [ sectionNamedWithSections "1121"
              [ sectionNamedWithSections "11211"
                [ sectionNamed "112111"
                ]
              ]
            , sectionNamed "1122"
            ]
          ]
        ])

    it "handles nested sections" $ do
      -- when
      let res = fromRight' $ parse' section $ unlines
            [ "* First Headline"
            , "Some paragraph"
            , ""
            , "** Second Headline"
            , "Another paragraph"
            , "*** Third Headline"
            , "Also paragraph"
            ]
      -- then
      res `shouldBe` Section
        { sectionHeading = NE.fromList [Plain "First", Whitespace " ", Plain "Headline"]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionProperties = Nothing
        , sectionTags = []
        , sectionTodoKeyword = Nothing
        , sectionPriority = Nothing
        , sectionDoc = OrgDoc
          { docBlocks = [([], Paragraph $ NE.fromList [Plain "Some", Whitespace " ", Plain "paragraph"])]
          , docSections =
            [ Section
              { sectionHeading = NE.fromList [Plain "Second", Whitespace " ", Plain "Headline"]
              , sectionPlanningInfo = emptyPlanningInfo
              , sectionProperties = Nothing
              , sectionTags = []
              , sectionTodoKeyword = Nothing
              , sectionPriority = Nothing
              , sectionDoc = OrgDoc
                { docBlocks =
                  [ ([], Paragraph $ NE.fromList
                    [ Plain "Another", Whitespace " ", Plain "paragraph" ])
                  ]
                , docSections =
                  [ Section
                    { sectionHeading = NE.fromList [Plain "Third", Whitespace " ", Plain "Headline"]
                    , sectionPlanningInfo = emptyPlanningInfo
                    , sectionProperties = Nothing
                    , sectionTags = []
                    , sectionTodoKeyword = Nothing
                    , sectionPriority = Nothing
                    , sectionDoc = OrgDoc
                      { docBlocks =
                        [ ( []
                          , Paragraph $ NE.fromList [Plain "Also", Whitespace " ", Plain "paragraph"]
                          )
                        ]
                      , docSections = []
                      }
                    }
                  ]
                }
              }
            ]
          }
        }

    it "parses section tags" $ do
      -- when
      let res = fromRight' $ parse' section $ unlines
            [ "* Headline :aTag:"
            ]
      -- then
      res `shouldBe` Section
        { sectionHeading = NE.fromList [Plain "Headline"]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionProperties = Nothing
        , sectionTags = ["aTag"]
        , sectionTodoKeyword = Nothing
        , sectionPriority = Nothing
        , sectionDoc = emptyDoc
        }

    it "parses section todo status" $ do
      -- when
      let res = fromRight' $ parse' section $ unlines
            [ "* TODO do this"
            ]
      -- then
      res `shouldBe` Section
        { sectionHeading = NE.fromList [Plain "do", Whitespace " ", Plain "this"]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionProperties = Nothing
        , sectionTags = []
        , sectionTodoKeyword = Just "TODO"
        , sectionPriority = Nothing
        , sectionDoc = emptyDoc
        }

    it "parses section todo status at beginning of headline but with extra letters" $ do
      -- when
      let res = fromRight' $ parse' section $ unlines
            [ "* TODOs do this"
            ]
      -- then
      res `shouldBe` Section
        { sectionHeading = NE.fromList
          [ Plain "TODOs"
          , Whitespace " "
          , Plain "do"
          , Whitespace " "
          , Plain "this"
          ]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionProperties = Nothing
        , sectionTags = []
        , sectionTodoKeyword = Nothing
        , sectionPriority = Nothing
        , sectionDoc = emptyDoc
        }

    it "parses priority" $ do
      -- when
      let res = parse' section $ unlines
            [ "* [#A] Todo"
            ]
      -- then
      res `shouldBe` Right Section
        { sectionHeading = NE.fromList
          [ Plain "Todo"
          ]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionProperties = Nothing
        , sectionTags = []
        , sectionTodoKeyword = Nothing
        , sectionPriority = Just 'A'
        , sectionDoc = emptyDoc
        }

    it "parses section' property drawer" $ do
      -- when
      let res = parse' section $ unlines
            [ "* Section"
            , ":PROPERTIES:"
            , ":PROPNAME: PROPVALUE"
            , ":END:"
            ]
      -- then
      res `shouldBe` Right Section
        { sectionHeading = NE.fromList [Plain "Section"]
        , sectionProperties = Just
          [ DrawerProperty "PROPNAME" "PROPVALUE"
          ]
        , sectionPlanningInfo = emptyPlanningInfo
        , sectionTags = []
        , sectionTodoKeyword = Nothing
        , sectionPriority = Nothing
        , sectionDoc = emptyDoc
        }

  describe "timestamp" $ do
    it "active" $ do
      -- when
      let res = parse' timestamp "<1234-12-01 Mon>"
      -- then
      res `shouldBe` Right (ActiveTimestamp 1234 December Day1 "Mon")

    it "inactive" $ do
      -- when
      let res = parse' timestamp "[1654-01-12 Tue]"
      -- then
      res `shouldBe` Right (InactiveTimestamp 1654 January Day12 "Tue")

  describe "property drawer" $ do
    it "empty" $ do
      -- when
      let res = parse' propertyDrawer $ unlines
            [ ":PROPERTIES:"
            , ":END:"
            ]
      -- then
      res `shouldBe` Right []

    it "normal property" $ do
      -- when
      let res = parse' propertyDrawer $ unlines
            [ ":PROPERTIES:"
            , ":A_NAME: this is a value"
            , ":END:"
            ]
      -- then
      res `shouldBe` Right [DrawerProperty "A_NAME" "this is a value"]

    it "empty property" $ do
      -- when
      let res = parse' propertyDrawer $ unlines
            [ ":PROPERTIES:"
            , ":A_NAME:"
            , ":END:"
            ]
      -- then
      res `shouldBe` Right [EmptyDrawerProperty "A_NAME"]

    it "property with plus at end" $ do
      -- when
      let res = parse' propertyDrawer $ unlines
            [ ":PROPERTIES:"
            , ":MY_P+: with value"
            , ":END:"
            ]
      -- then
      res `shouldBe` Right [DrawerPropertyPlus "MY_P" "with value"]

    it "empty property with plus at end" $ do
      -- when
      let res = parse' propertyDrawer $ unlines
            [ ":PROPERTIES:"
            , ":SOME_NAME+:"
            , ":END:"
            ]
      -- then
      res `shouldBe` Right [EmptyDrawerPropertyPlus "SOME_NAME"]

  describe "meta" $ do
    describe "property" $ do
      it "parses a title" $ do
        -- when
        let res = fromRight' $ parse' property
              "#+TITLE: document title"
        -- then
        res `shouldBe` ("TITLE", "document title")

      it "parses only one pair" $ do
        -- when
        let res = fromRight' $ parse' property
              "#+KEY1: first value\n\
              \#+key2: second value"
        -- then
        res `shouldBe` ("KEY1", "first value")

    describe "properties" $ do
      it "parses multiple properties" $ do
        -- when
        let res = fromRight' $ parse' properties
              "#+key1: first value\n\
              \#+key2: second value"
        -- then
        res `shouldBe` M.fromList
          [ ("key1", "first value")
          , ("key2", "second value")
          ]

  describe "doc" $ do
    it "paragraph" $ do
      -- when
      let res = parse' doc "Hello there!"
      -- then
      res `shouldBe` Right OrgDoc
        { docBlocks =
          [ ( []
            , Paragraph $ NE.fromList
              [ Plain "Hello"
              , Whitespace " "
              , Plain "there"
              , Punct "!"
              ]
            )
          ]
        , docSections = []
        }

    describe "block" $ do
      it "paragraph" $ do
        -- when
        let res = parse' block
              "Hello"
        -- then
        res `shouldBe` Right ([], Paragraph (NE.fromList [Plain "Hello"]))

      describe "block element" $ do
        it "comment" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN_COMMENT"
                , "some content"
                , "on multiple lines"
                , "#+END_COMMENT"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $ Comment Nothing "some content\non multiple lines")

        it "example" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN_EXAMPLE"
                , "some content"
                , "on multiple lines"
                , "#+END_EXAMPLE"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $ Example Nothing "some content\non multiple lines")

        it "verse" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN_VERSE"
                , "some content"
                , "on multiple lines"
                , "#+END_VERSE"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $ Verse Nothing "some content\non multiple lines")

        it "src" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN_SRC python"
                , "def fn():"
                , "  pass"
                , "#+END_SRC"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $ Src "python" "def fn():\n  pass")

        it "export" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN_EXPORT singleword"
                , "something"
                , "inside"
                , "#+END_EXPORT"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $ Export "singleword" "something\ninside")

        it "lowercase export" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+begin_export aword"
                , "hi"
                , "#+end_export"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $ Export "aword" "hi")

        it "empty" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN_SRC javascript"
                , "#+END_SRC"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $ Src "javascript" "")

        it "with stopping text in content" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN_EXAMPLE"
                , "some content containing #+END_EXAMPLE"
                , "#+END_EXAMPLE"
                ]
          -- then
          res `shouldBe` Right ([], BlockElement $
            Example Nothing "some content containing #+END_EXAMPLE")

      describe "dynamic block" $ do
        it "basic" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN: name"
                , "Some content"
                , "on two lines"
                , "#+END:"
                ]
          -- then
          res `shouldBe` Right ([], DynamicBlock "name" M.empty "Some content\non two lines")

        it "with parameters" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+BEGIN: my-block :par1 val :par2 value"
                , "Wooooow"
                , "#+END:"
                ]
          -- then
          res `shouldBe` Right ([], DynamicBlock
            "my-block"
            (M.fromList [("par1", "val"), ("par2", "value")])
            "Wooooow"
            )

      it "table" $ do
        -- when
        let res = parse' block $ unlines
              [ "| Column 1 | Column 2 |"
              , "|----------+----------|"
              , "| 11       |   21     |"
              , "|          |    hello |"
              ]
        -- then
        res `shouldBe` Right ([], Table (NE.fromList
          [ Row $ NE.fromList
            [ Column $ NE.fromList [Plain "Column", Whitespace " ", Plain "1"]
            , Column $ NE.fromList [Plain "Column", Whitespace " ", Plain "2"]
            ]
          , Break
          , Row $ NE.fromList
            [ Column $ NE.fromList [Plain "11"]
            , Column $ NE.fromList [Plain "21"]
            ]
          , Row $ NE.fromList
            [ Empty
            , Column $ NE.fromList [Plain "hello"]
            ]
          ]))

      describe "list" $ do
        it "basic" $ do
          -- when
          let res = parse' block $ unlines
                [ "- Hello"
                ]
          -- then
          res `shouldBe` Right ([], List (ListItems $ NE.fromList
            [ Item (NE.fromList [Plain "Hello"]) Nothing
            ]))

        it "with + as marker" $ do
          -- when
          let res = parse' block $ unlines
                [ "+ Hello"
                ]
          -- then
          res `shouldBe` Right ([], List (ListItems $ NE.fromList
            [ Item (NE.fromList [Plain "Hello"]) Nothing
            ]))

        it "nested, with * as marker" $ do
          -- when
          let res = parse' block $ unlines
                [ "+ Hello"
                , "  * nested"
                ]
          -- then
          res `shouldBe` Right ([], List (ListItems $ NE.fromList
            [ Item (NE.fromList [Plain "Hello"]) $ Just $ ListItems $ NE.fromList
              [ Item (NE.fromList [Plain "nested"]) Nothing
              ]
            ]))

        it "with multiple items" $ do
          -- when
          let res = parse' block $ unlines
                [ "- First"
                , "- Second"
                ]
          -- then
          res `shouldBe` Right ([], List (ListItems $ NE.fromList
            [ Item (NE.fromList [Plain "First"]) Nothing
            , Item (NE.fromList [Plain "Second"]) Nothing
            ]))

        it "with sub-list" $ do
          -- when
          let res = parse' block $ unlines
                [ "- Top"
                , "  - Sub1"
                , "  - Sub2"
                ]
          -- then
          res `shouldBe` Right ([], List (ListItems $ NE.fromList
            [ Item (NE.fromList [Plain "Top"]) $ Just $ ListItems $ NE.fromList
              [ Item (NE.fromList [Plain "Sub1"]) Nothing
              , Item (NE.fromList [Plain "Sub2"]) Nothing
              ]
            ]))

        it "with description item" $ do
          -- when
          let res = parse' block $ unlines ["- Item :: Description"]
          -- then
          res `shouldBe` Right ([], List (ListItems $ NE.fromList
            [ DescriptionItem
                (NE.fromList [Plain "Item"])
                (NE.fromList [Plain "Description"])
                Nothing
            ]))

      describe "can have affiliated keywords" $ do
        it "paragraph" $ do
          -- when
          let res = parse' block $ unlines
                [ "#+NAME: some name"
                , "#+ATTR_DND: backend option"
                , "A paragraph."
                ]
          -- then
          res `shouldBe` Right
            ( [ Name "some name"
              , AttrAffiliatedKeyword "DND" "backend option"
              ]
              , Paragraph $ NE.fromList
                [ Plain "A"
                , Whitespace " "
                , Plain "paragraph"
                , Punct "."
                ]
            )

      it "paragraph can be multi-line" $ do
        -- when
        let res = parse' block $ unlines
              [ "First line"
              , "Second line"
              ]
        -- then
        res `shouldBe` Right ([], Paragraph $ NE.fromList
          [ Plain "First"
          , Whitespace " "
          , Plain "line"
          , Whitespace "\n"
          , Plain "Second"
          , Whitespace " "
          , Plain "line"
          ])

      it "try tricking bold" $ do
        -- when
        let res = fromRight' $ parse' block
              "*not *bold"
        -- then
        res `shouldBe` ([], Paragraph (NE.fromList
          [Plain "*not", Whitespace " ", Plain "*bold"]))

      it "bold - pretended nesting" $ do
        -- when
        let res = fromRight' $ parse' block
              "*This *is* nested*"
        -- then
        res `shouldBe` ([], Paragraph (NE.fromList
          [ Bold (NE.fromList
              [ Plain "This"
              , Whitespace " "
              , Plain "*is"
              ])
          , Whitespace " "
          , Plain "nested*"
          ]))

      it "bold - end before punctuation" $ do
        -- when
        let res = parse' block "Some *bold text*."
        -- then
        res `shouldBe` Right ([], Paragraph $ NE.fromList
          [ Plain "Some"
          , Whitespace " "
          , Bold $ NE.fromList
            [ Plain "bold"
            , Whitespace " "
            , Plain "text"
            ]
          , Punct "."
          ])

    describe "textElement" $ do
      it "plain" $ do
        -- when
        let res = fromRight' $ parse' textElement "Hello"
        -- then
        head res `shouldBe` Plain "Hello"

      it "plain - single asterisk in middle" $ do
        -- when
        let res = fromRight' $ parse' textElement "ast*erisk"
        -- then
        head res `shouldBe` Plain "ast*erisk"

      it "plain - single asterisk at start" $ do
        -- when
        let res = fromRight' $ parse' textElement "*asterisk"
        -- then
        head res `shouldBe` Plain "*asterisk"

      it "plain - end asterisk in word" $ do
        -- when
        let res = fromRight' $ parse' textElement "*aster*isk"
        -- then
        head res `shouldBe` Plain "*aster*isk"

      it "bold" $ do
        -- when
        let res = fromRight' $ parse' textElement "*Hello*"
        -- then
        head res `shouldBe` Bold (NE.fromList [Plain "Hello"])

      it "bold - over punctuation" $ do
        -- when
        let res = fromRight' $ parse' textElement
              "*This. is. bold.*"
        -- then
        head res `shouldBe` Bold (NE.fromList
          [ Plain "This"
          , Punct "."
          , Whitespace " "
          , Plain "is"
          , Punct "."
          , Whitespace " "
          , Plain "bold"
          , Punct "."
          ])

      it "link" $ do
        -- when
        let res = fromRight' $ parse' textElement "[[Headline]]"
        -- then
        head res `shouldBe` Link (URL "Headline") Nothing

      it "link with description" $ do
        -- when
        let res = fromRight' $ parse' textElement "[[attachment:a file][The Name]]"
        -- then
        head res `shouldBe` Link (URL "attachment:a file") (Just $ NE.fromList
          [Plain "The", Whitespace " ", Plain "Name"])

      it "monospace" $ do
        -- when
        let res = fromRight' $ parse' textElement "~some monospace text~"
        -- then
        head res `shouldBe` Monospace (NE.fromList
          [ Plain "some"
          , Whitespace " "
          , Plain "monospace"
          , Whitespace " "
          , Plain "text"
          ])

      it "complex monospace" $ do
        -- when
        let res = fromRight' $ parse' textElement "~~/org~"
        -- then
        head res `shouldBe` Monospace (NE.fromList
          [ Plain "~/org"
          ])

      it "verbatim" $ do
        -- when
        let res = parse' textElement "=verbatim="
        -- then
        res `shouldBe` Right [Verbatim $ NE.fromList
          [ Plain "verbatim"
          ]]

      it "strikethrough" $ do
        -- when
        let res = parse' textElement "+strikethrough+"
        -- then
        res `shouldBe` Right [Strikethrough $ NE.fromList
          [ Plain "strikethrough"
          ]]

      it "bold - including asterisk" $ do
        -- when
        let res = fromRight' $ parse' textElement "*Th*is*"
        -- then
        head res `shouldBe` Bold (NE.fromList [Plain "Th*is"])

      it "italic" $ do
        -- when
        let res = fromRight' $ parse' textElement "/italic/"
        -- then
        head res `shouldBe` Italic (NE.fromList [Plain "italic"])

      it "italic and bold" $ do
        -- when
        let res = fromRight' $ parse' textElement "/italic *bold* more/"
        -- then
        head res `shouldBe` Italic (NE.fromList
          [ Plain "italic"
          , Whitespace " "
          , Bold (NE.fromList [Plain "bold"])
          , Whitespace " "
          , Plain "more"
          ])

