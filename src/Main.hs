{-# LANGUAGE LambdaCase #-}
module Main where

import           Text.Parsec.Error
import           System.Environment
import           System.Process
import           System.Directory
import           Control.DeepSeq
import           GHC.IO.Encoding
import           System.IO

import           Org
import           Latex

encoding :: IO TextEncoding
encoding = mkTextEncoding "UTF-8//ROUNDTRIP"
  
main :: IO ()
main = getArgs >>= \case
  ["--pdf", dir] -> makePdf dir
  [dir, "--pdf"] -> makePdf dir
  [dir] -> selectNotes dir >>= \case
    [] -> undefined
    [noteName] -> getFileContent dir noteName >>= printConverted
    _ -> undefined
  _ -> getInputStream >>= printConverted

selectNotes :: String -> IO [String]
selectNotes dir = do
  notes <- listDirectory dir
  lines <$> readProcess "fzf" [] (unlines notes)

makePdf :: String -> IO ()
makePdf dir = do
  selectedNotes <- selectNotes dir
  mapM (\name -> (getFileContent dir name) >>= (\orgContent -> makePdf' orgContent name)) selectedNotes
  pure ()

makePdf' :: String -> String -> IO ()
makePdf' org outputFileName = do
  case convert org of
    Left err -> error $ show err
    Right latex -> do
      readProcess "pdflatex" ["-jobname", outputFileName] $!! latex
      pure ()

printConverted :: String -> IO ()
printConverted input = case convert input of
  Left err -> error $ show err
  Right out -> do
    enc <- encoding
    hSetEncoding stdout enc
    putStrLn preText
    putStrLn out
    putStrLn postText

getInputStream :: IO String
getInputStream = do
  enc <- encoding
  hSetEncoding stdin enc
  getContents

getFileContent :: String -> String -> IO String
getFileContent dir fileName = do
  let filePath = dir <> "/" <> fileName
  enc <- encoding
  handle <- openFile filePath ReadMode
  hSetEncoding handle enc
  content <- hGetContents handle
  content `deepseq` hClose handle
  return content

convert :: String -> Either ParseError String
convert orgFileContents =
  case org orgFileContents of
    Left err -> Left err
    Right orgFile -> Right $ convertOrgASTToText orgFile
