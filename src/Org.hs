{-# LANGUAGE
    RankNTypes
  , KindSignatures
  , TypeApplications
  , ScopedTypeVariables
  , DataKinds
  , MultiParamTypeClasses
  , FunctionalDependencies
  , AllowAmbiguousTypes
  , FlexibleInstances
  , TypeFamilies
  , OverloadedLabels
  , DeriveGeneric
  , FlexibleContexts #-}
module Org where

import           Data.Maybe (fromMaybe, catMaybes)
import           Data.Functor ((<&>))
import           Data.Functor.Identity (Identity)
import qualified Data.Map.Strict as M
import qualified Data.List.NonEmpty as NE
import           Text.Parsec hiding (Empty)
import           Data.Char
import           Control.Monad (void)
import           Data.List (intercalate)

import           Data.Generics.Product.Fields
import           GHC.Generics
import qualified Debug.Trace as Debug

org :: String -> Either ParseError OrgFile
org input =
  case runParser orgFileParser defaultParserState "" input of
      Left e -> Left e
      Right o -> Right o

data OrgFile = OrgFile
  { orgProperties :: Maybe PropertyDrawer
  , orgMeta :: M.Map String String
  , orgDoc  :: OrgDoc
  } deriving (Eq, Show)

data OrgDoc = OrgDoc
  { docBlocks   :: [([AffiliatedKeyword], Block)]
  , docSections :: [Section]
  } deriving (Eq, Show)

data AffiliatedKeyword
  = Caption String
  | Header String
  | Name String
  | Plot String
  | Results String
  | AttrAffiliatedKeyword String String
  deriving (Eq, Show)

data Block
  = Paragraph (NE.NonEmpty TextElement)
  | Table (NE.NonEmpty Row)
  | List ListItems
  | DynamicBlock String (M.Map String String) String
  | BlockElement BlockElement
  deriving (Eq, Show)

data BlockElement
  = Comment (Maybe String) String
  | Example (Maybe String) String
  | Verse (Maybe String) String
  | Export String String
  | Src String String
  deriving (Eq, Show)

data Row = Break | Row (NE.NonEmpty Org.Column)
  deriving (Eq, Show)

data Column = Empty | Column (NE.NonEmpty TextElement)
  deriving (Eq, Show)

data TextElement
  = Bold (NE.NonEmpty TextElement)
  | Italic (NE.NonEmpty TextElement)
  | Monospace (NE.NonEmpty TextElement)
  | Verbatim (NE.NonEmpty TextElement)
  | Strikethrough (NE.NonEmpty TextElement)
  | Link URL (Maybe (NE.NonEmpty TextElement))
  | Plain String
  | Punct String
  | Whitespace String
  deriving (Eq, Show)

data Section = Section
  { sectionHeading :: NE.NonEmpty TextElement
  , sectionPlanningInfo :: PlanningInfo
  , sectionTags :: [String]
  , sectionTodoKeyword :: Maybe String
  , sectionPriority :: Maybe Char
  , sectionProperties :: Maybe PropertyDrawer
  , sectionDoc :: OrgDoc
  } deriving (Eq, Show)

data PlanningInfo = PlanningInfo
  { scheduled :: Maybe Timestamp
  } deriving (Eq, Show)

data Timestamp
  = ActiveTimestamp
    { year :: Int
    , month :: Month
    , day :: Day
    , dayOfWeek :: String
    }
  | InactiveTimestamp
    { year :: Int
    , month :: Month
    , day :: Day
    , dayOfWeek :: String
    }
  deriving (Eq, Show)

data Month
  = January
  | February
  | March
  | April
  | May
  | June
  | July
  | August
  | September
  | October
  | November
  | December
  deriving (Eq, Show)

data Day
  = Day1
  | Day2
  | Day3
  | Day4
  | Day5
  | Day6
  | Day7
  | Day8
  | Day9
  | Day10
  | Day11
  | Day12
  | Day13
  | Day14
  | Day15
  | Day16
  | Day17
  | Day18
  | Day19
  | Day20
  | Day21
  | Day22
  | Day23
  | Day24
  | Day25
  | Day26
  | Day27
  | Day28
  | Day29
  | Day30
  | Day31
  deriving (Eq, Show)

type PropertyDrawer = [PropertyDrawerProperty]

data PropertyDrawerProperty
  = DrawerProperty String String
  | EmptyDrawerProperty String
  | DrawerPropertyPlus String String
  | EmptyDrawerPropertyPlus String
  deriving (Eq, Show)

newtype ListItems = ListItems (NE.NonEmpty Item)
  deriving (Eq, Show)

data Item
  = Item (NE.NonEmpty TextElement) (Maybe ListItems)
  | DescriptionItem (NE.NonEmpty TextElement) (NE.NonEmpty TextElement) (Maybe ListItems)
  deriving (Eq, Show)

newtype URL = URL String
  deriving (Eq, Show)

-----------------------------------------------------------------------

emptyDoc :: OrgDoc
emptyDoc = OrgDoc
  { docBlocks = []
  , docSections = []
  }

emptyPlanningInfo :: PlanningInfo
emptyPlanningInfo = PlanningInfo Nothing

data ParserState = ParserState
  { isInBold :: Bool
  , isInItalic :: Bool
  , isInMonospace :: Bool
  , isInVerbatim :: Bool
  , isInStrikethrough :: Bool
  , sectionLevel :: Int
  , todoKeywords :: [String]
  , listLevel :: Int
  } deriving (Generic)

defaultParserState :: ParserState
defaultParserState = ParserState
  { isInBold = False
  , isInItalic = False
  , isInMonospace = False
  , isInVerbatim = False
  , isInStrikethrough = False
  , sectionLevel = 0
  , todoKeywords = ["TODO", "DONE"]
  , listLevel = 0
  }

type OrgParser t = ParsecT String ParserState Identity t

atLeastOne
  :: forall s (m :: * -> *) t u a
  .  Stream s m t
  => ParsecT s u m a -> ParsecT s u m [a]
atLeastOne = many1

followedBy
  :: forall s (m :: * -> *) t u a
  .  Stream s m t
  => ParsecT s u m a -> ParsecT s u m a
followedBy = lookAhead

whitespaceChar :: OrgParser Char
whitespaceChar = satisfy $ \c -> isSpace c && c /= '\n'

anyCaseChar :: Char -> OrgParser Char
anyCaseChar c = try (char $ toLower c) <|> char (toUpper c)

anyCaseString :: String -> OrgParser String
anyCaseString = mapM anyCaseChar

orgFileParser :: OrgParser OrgFile
orgFileParser = do
  propertyDrawer' <- optionMaybe propertyDrawer
  skipMany newline
  properties' <- properties
  skipMany newline
  doc' <- doc
  eof
  return OrgFile
    { orgProperties = propertyDrawer'
    , orgMeta = properties'
    , orgDoc = doc'
    }

properties :: OrgParser (M.Map String String)
properties = M.fromList <$> many property

property :: OrgParser (String, String)
property = do
  string "#+"
  key <- atLeastOne $ noneOf ":"
  char ':'
  many whitespaceChar
  value <- many $ noneOf "\n"
  optional newline

  pure (key, value)

doc :: OrgParser OrgDoc
doc = do
  blocks <- many $ try $ do
    b <- block
    skipMany newline
    pure b

  sections <- many $ try $ do
    s <- section
    skipMany newline
    pure s

  optional newline

  pure OrgDoc
    { docBlocks = blocks
    , docSections = sections
    }

section :: OrgParser Section
section = do
  nestedSectionStart

  state <- getState
  let level = getField @"sectionLevel" state
  modifyState $ setField @"sectionLevel" $ level + 1

  keyword <- optionMaybe $ try $ do
    keyword <- todoKeyword
    whitespaceChar
    pure keyword

  priority <- optionMaybe $ try $ do
    string "[#"
    c <- alphaNum
    char ']'
    whitespaceChar
    pure c

  elements <- manyTill textElement (lookAhead $ try (void tags <|> void newline <|> eof))

  if null elements
    then unexpected "Empty headline"
    else do
      tags' <- optionMaybe tags

      planningInfo <- optionMaybe $ try $ do
        newline
        many whitespaceChar
        fstInfo <- info
        otherInfos <- many $ try $ do
          atLeastOne whitespaceChar
          info
        let allInfos = fstInfo:otherInfos
        let infos = M.fromList allInfos
        pure $ PlanningInfo
          { scheduled = infos M.!? "scheduled"
          }

      propertyDrawer' <- optionMaybe $ try $ do
        newline
        propertyDrawer

      doc' <- optionMaybe $ try $ do
        atLeastOne newline
        doc

      modifyState $ setField @"sectionLevel" level

      pure Section
        { sectionHeading = NE.fromList $ concat elements
        , sectionPlanningInfo = fromMaybe emptyPlanningInfo planningInfo
        , sectionTags = fromMaybe [] tags'
        , sectionTodoKeyword = keyword
        , sectionPriority = priority
        , sectionProperties = propertyDrawer'
        , sectionDoc = fromMaybe emptyDoc doc'
        }
  where
    info = do
      keyword <- choice $ map (try . anyCaseString) ["deadline", "scheduled", "closed"]
      string ": "
      timestamp' <- timestamp
      pure (map toLower keyword, timestamp')

timestamp :: OrgParser Timestamp
timestamp =
  choice $ map try $ 
    [ activeTimestamp
    , inactiveTimestamp
    ]
  where
    activeTimestamp = do
      char '<'
      year <- yearP
      char '-'
      month <- monthP
      char '-'
      day <- dayP
      char ' '
      dayOfWeek <- manyTill (satisfy (/='>')) $ char '>'
      pure ActiveTimestamp
        { year = year
        , month = monthFromString month
        , day = dayFromString day
        , dayOfWeek = dayOfWeek
        }

    inactiveTimestamp = do
      char '['
      year <- yearP
      char '-'
      month <- monthP
      char '-'
      day <- dayP
      char ' '
      dayOfWeek <- manyTill (satisfy (/=']')) $ char ']'
      pure InactiveTimestamp
        { year = year
        , month = monthFromString month
        , day = dayFromString day
        , dayOfWeek = dayOfWeek
        }

    yearP = read <$> do
      fst <- nonZeroDigit
      rest <- many digit
      pure $ fst:rest

    monthP = choice $ map try
      [ do
        char '1'
        snd <- char '0' <|> char '1' <|> char '2'
        pure ['1', snd]
      , do
        optional $ char '0'
        d <- nonZeroDigit
        pure [d]
      ]

    dayP = choice $ map try
      [ do
        first <- choice $ map (try . char) "12"
        second <- digit
        pure [first, second]
      , do
        char '3'
        snd <- char '0' <|> char '1'
        pure ['3', snd]
      , do
        optional $ char '0'
        d <- nonZeroDigit
        pure [d]
      ]

nonZeroDigit :: OrgParser Char
nonZeroDigit = oneOf "123456789"

monthFromString :: String -> Month
monthFromString "1" = January
monthFromString "2" = February
monthFromString "3" = March
monthFromString "4" = April
monthFromString "5" = May
monthFromString "6" = June
monthFromString "7" = July
monthFromString "8" = August
monthFromString "9" = September
monthFromString "10" = October
monthFromString "11" = November
monthFromString "12" = December
monthFromString m = error $ show m ++ " is not a valid month."

dayFromString :: String -> Day
dayFromString "1" = Day1
dayFromString "2" = Day2
dayFromString "3" = Day3
dayFromString "4" = Day4
dayFromString "5" = Day5
dayFromString "6" = Day6
dayFromString "7" = Day7
dayFromString "8" = Day8
dayFromString "9" = Day9
dayFromString "10" = Day10
dayFromString "11" = Day11
dayFromString "12" = Day12
dayFromString "13" = Day13
dayFromString "14" = Day14
dayFromString "15" = Day15
dayFromString "16" = Day16
dayFromString "17" = Day17
dayFromString "18" = Day18
dayFromString "19" = Day19
dayFromString "20" = Day20
dayFromString "21" = Day21
dayFromString "22" = Day22
dayFromString "23" = Day23
dayFromString "24" = Day24
dayFromString "25" = Day25
dayFromString "26" = Day26
dayFromString "27" = Day27
dayFromString "28" = Day28
dayFromString "29" = Day29
dayFromString "30" = Day30
dayFromString "31" = Day31
dayFromString d = error $ show d ++ "is not a valid number for a day in a month"

propertyDrawer :: OrgParser PropertyDrawer
propertyDrawer = do
  anyCaseString ":PROPERTIES:"
  newline
  propertyDrawerProperties <- many $ try $ do
    p <- propertyDrawerProperty
    newline
    pure p
  anyCaseString ":END:"
  pure propertyDrawerProperties
  where
    propertyDrawerProperty = do
      char ':'
      notFollowedBy $ try $ anyCaseString "END:\n"
      name <- manyTill (satisfy $ not . isSpace) $ try $ char ':'
      value <- optionMaybe $ try $ do
        atLeastOne whitespaceChar
        manyTill anyChar $ followedBy newline
      case reverse name of
        '+':'+':_ -> unexpected "Property drawer names cannot end with `+`"
        "+" -> unexpected "Empty property drawer property name"
        "" -> unexpected "Empty property drawer property name"
        '+':name -> case value of
          Nothing -> pure $ EmptyDrawerPropertyPlus $ reverse name
          Just value -> pure $ DrawerPropertyPlus (reverse name) value
        name -> case value of
          Nothing -> pure $ EmptyDrawerProperty $ reverse name
          Just value -> pure $ DrawerProperty (reverse name) value

todoKeyword :: OrgParser String
todoKeyword = do
  state <- getState
  let keywords = getField @"todoKeywords" state
  choice $ map (try . string) keywords

tags :: OrgParser [String]
tags = do
  whitespaceChar
  char ':'
  -- endBy tag $ char ':'
  many $ do
    t <- tag
    char ':'
    pure t

tag :: OrgParser String
tag = atLeastOne $ alphaNum <|> oneOf "_@"

inline :: OrgParser (NE.NonEmpty TextElement)
inline = do
  notFollowedBy $ try $ do
    nestedSectionStart
  NE.fromList . concat <$> atLeastOne textElement

block :: OrgParser ([AffiliatedKeyword], Block)
block = do
  block' False
  where
    block' isInParagraph = do
      affiliatedKeywords <- many $ try affiliatedKeyword

      notFollowedBy $ do
        atLeastOne (char '*')
        whitespaceChar

      block' <- choice $ map try $ catMaybes
        [ Just list
        , Just table
        , Just blockElement
        , Just dynamicBlock
        , if isInParagraph then Nothing else Just paragraph
        ]

      pure (affiliatedKeywords, block')

    blockElement = BlockElement <$> do
      anyCaseString "#+BEGIN_"
      name <- manyTill anyChar $ followedBy $ satisfy isSpace
      blockData <- optionMaybe $ do
        atLeastOne whitespaceChar
        manyTill anyChar $ followedBy newline
      content' <- manyTill anyChar $ try $ do
        newline
        anyCaseString $ "#+END_" ++ name
        followedBy newline

      let (_:content) = if content' == "" then "\n" else content'

      pure $ case map toUpper name of
        "COMMENT" -> Comment blockData content
        "EXAMPLE" -> Example blockData content
        "VERSE" -> Verse blockData content
        "EXPORT" -> case blockData of
          Nothing -> error "Encountered invalid export block: missing data"
          Just blockData' -> Export blockData' content
        "SRC" -> case blockData of
          Nothing -> error "Encountered invalid src block: missing data"
          Just blockData' -> Src blockData' content
        _ -> Debug.trace name undefined

    dynamicBlock = do
      string "#+BEGIN: "
      name <- atLeastOne $ satisfy $ not . isSpace
      parameters <- many $ do
        many whitespaceChar
        parameter
      newline
      content <- manyTill anyChar $ try $ do
        newline
        string "#+END:"
        newline
      pure $ DynamicBlock name (M.fromList parameters) content
      where
        parameter = do
          char ':'
          key <- atLeastOne $ satisfy $ not . isSpace
          many whitespaceChar
          value <- atLeastOne $ satisfy $ not . isSpace
          pure (key, value)

    affiliatedKeyword = do
      let basicAffiliatedKeywords =
            [ ("CAPTION", Caption)
            , ("HEADER", Header)
            , ("NAME", Name)
            , ("Plot", Plot)
            , ("RESULTS", Results)
            ]
      choice $
        map (\(s, c) -> try $ basicAffiliatedKeyword s c) basicAffiliatedKeywords ++
        [attrAffiliatedKeyword]
      where
        basicAffiliatedKeyword str constructor = do
          string "#+"
          keyword <- try $ string str
          char ':'
          many whitespaceChar
          value <- manyTill anyChar $ try newline
          pure $ constructor value

        attrAffiliatedKeyword = do
          string "#+ATTR_"
          backend <- manyTill anyChar $ try $ char ':'
          many whitespaceChar
          value <- manyTill anyChar $ try newline
          pure $ AttrAffiliatedKeyword backend value

    table = do
      firstRow <- row
      rows <- many $ try $ do
        newline
        row
      pure $ Table $ firstRow NE.:| rows
      where
        row = try break <|> fields

        break = do
          char '|'
          atLeastOne $ char '-'
          atLeastOne $ do
            char '+'
            atLeastOne $ char '-'
          char '|'
          pure Break

        fields = do
          char '|'
          Row . NE.fromList <$> do atLeastOne field

        field = try emptyField <|> filledField

        emptyField = do
          atLeastOne whitespaceChar
          char '|'
          pure Empty

        filledField = do
          atLeastOne whitespaceChar
          content <- manyTill textElement $ try $ do
            atLeastOne whitespaceChar
            char '|'
          pure $ Column $ NE.fromList $ concat content

    list = do
      firstListItem <- listItem
      otherListItems <- many $ try $ do
        newline
        listItem
      pure $ List $ ListItems $ firstListItem NE.:| otherListItems
      where
        listItem = do
          state <- getState
          let listLevel = getField @"listLevel" state
          count (listLevel * 2) $ char ' '

          choice $ map (try . string) $
            [ "- "
            , "+ "
            ] ++ (["* " | listLevel > 0])
          item <- try descriptionItem <|> simpleItem

          modifyState $ setField @"listLevel" $ listLevel + 1
          subItems <- many $ try $ do
            newline
            listItem
          modifyState $ setField @"listLevel" listLevel

          pure $ item $ case subItems of
            [] -> Nothing
            _ -> Just $ ListItems $ NE.fromList subItems

        descriptionItem = do
          name <- manyTill textElement $ try $ do
            optional whitespaceChar
            string "::"
          optional whitespaceChar
          DescriptionItem
            (NE.fromList $ concat name) <$> inline

        simpleItem = do
          Item <$> inline

    paragraph = do
      fstLine <- inline
      otherLines <- many $ try $ do
        newline
        notFollowedBy $ choice $ map try
          [ void $ block' True
          , nestedSectionStart
          , sameSectionStart
          , anyPreviousSectionStart
          ]
        inline
      pure $ Paragraph $ concatIntersperseNE (Whitespace "\n") $ fstLine:otherLines

concatIntersperseNE :: a -> [NE.NonEmpty a] -> NE.NonEmpty a
concatIntersperseNE elem lists = NE.fromList $ intercalate [elem] (map NE.toList lists)

nestedSectionStart :: OrgParser ()
nestedSectionStart = do
  state <- getState
  let nextLevel = 1 + getField @"sectionLevel" state
  sectionStart nextLevel

sameSectionStart :: OrgParser ()
sameSectionStart = do
  state <- getState
  let currentLevel = getField @"sectionLevel" state
  sectionStart currentLevel

anyPreviousSectionStart :: OrgParser ()
anyPreviousSectionStart = do
  state <- getState
  let previousLevel = getField @"sectionLevel" state
  choice $ map try $ [sectionStart n | n <- [0..previousLevel]]

sectionStart :: Int -> OrgParser ()
sectionStart level = do
  count level $ char '*'
  whitespaceChar
  pure ()

formattings :: [(NE.NonEmpty TextElement -> TextElement, ParserState -> Bool, Bool -> ParserState -> ParserState, Char)]
formattings =
  [ (Bold, isInBold, setField @"isInBold", '*')
  , (Italic, isInItalic, setField @"isInItalic", '/')
  , (Monospace, isInMonospace, setField @"isInMonospace", '~')
  , (Verbatim, isInVerbatim, setField @"isInVerbatim", '=')
  , (Strikethrough, isInStrikethrough, setField @"isInStrikethrough", '+')
  ]

textElement :: OrgParser [TextElement]
textElement = do
  w <- choice $ map try $
    link :
    map
      formatted
      formattings
    ++ [ plain
    , punctuation
    , whitespace
    ]
  pure [w]
  where
    link = do
      string "[["
      destination <- atLeastOne $ noneOf "]\n"
      char ']'
      description <- optionMaybe $ try linkDescription
      char ']'
      pure $ Link (URL destination) description
      where
        linkDescription = do
          char '['
          description <- manyTill textElement $ followedBy $ char ']'
          char ']'
          pure $ NE.fromList $ concat description

    formatted
      ::  ( NE.NonEmpty TextElement -> TextElement
          , ParserState -> Bool
          , Bool -> ParserState -> ParserState
          , Char)
      -> OrgParser TextElement
    formatted (constructor, getIsIn, setIsIn, symbol) = constructor <$> do
      state <- getState
      if getIsIn state
        then unexpected "cannot nest formatted text"
        else do
          char symbol

          modifyState $ setIsIn True

          firstTextElement <- plain <|> punctuation
          textElements <- many textElement <&> concat
          stoppingChar symbol

          modifyState $ setIsIn False

          pure $ firstTextElement NE.:| textElements

    plain = do
      state <- getState

      let allowedParsers = alphaNum : map
            (\(_, isIn, _, c) -> if isIn state then notStoppingChar c else char c)
            formattings

      characters <- atLeastOne $ choice allowedParsers

      pure $ Plain characters

    punctuation = do
      let fth = \(_,_,_,c) -> c
      punct <- atLeastOne $ satisfy $ \c ->
        not (isAlphaNum c || isSpace c || c `elem` map fth formattings)
      pure $ Punct punct

    whitespace = Whitespace <$> atLeastOne whitespaceChar

    stoppingChar :: Char -> OrgParser Char
    stoppingChar c = try $ do
      char c
      followedBy $ eof <|> void whitespace <|> void punctuation
        -- void punctuation <|> void whitespace <|> eof
      pure c

    notStoppingChar :: Char -> OrgParser Char
    notStoppingChar c = try $ do
      isStoppingNext <- optionMaybe $ followedBy $ stoppingChar c
      case isStoppingNext of
        Just _ -> unexpected "is a valid stopper"
        Nothing -> char c

